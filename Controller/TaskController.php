<?php

namespace App\Controller;

use App\Core\Auth;
use App\Core\Util;
use App\Exception\ValidationException;
use App\Model\Task;
use App\Model\User;
use App\Repository\TaskRepository;
use App\Repository\UserRepository;

class TaskController extends BaseController
{
    /** @var TaskRepository */
    private $repository;

    public function __construct()
    {
        parent::__construct();

        $this->repository = new TaskRepository();
    }

    public function index()
    {
        echo $this->util->render('index', ['create_task_link' => Util::buildUrl(['p' => 'task', 'a' => 'create'])]);
    }

    public function list()
    {
        $order = [];
        $requestFilter = $this->request->get['order'] ?? [];
        $orderFields = $this->repository->getOrderFields();
        foreach ($requestFilter as $row) {
            $order[] = [
                'column' => $orderFields[$row['column']],
                'dir' => $row['dir'],
            ];
        }

        $data = $this->repository->getFiltered([], $order, $this->request->get['start'] ?? 0,
            $this->request->get['length'] ?? null);
        $preparedData = [];
        foreach ($data['data'] as $row) {
            $row['completed'] = !empty($row['completed']) ? '<b>yes</b>' : '';
            $row['admin_updated'] = !empty($row['admin_updated']) ? '<b>yes</b>' : '';
            $link = Util::buildUrl(['p' => 'task', 'a' => 'edit', 'id' => $row['id']]);
            $row['content'] = htmlspecialchars($row['content']);
            $row['id'] = $this->util->render('button_link', ['link' => $link, 'text' => 'Edit']);
            $preparedData[] = $row;
        }
        $json = [
            'recordsTotal' => $data['total'],
            'recordsFiltered' => $data['total'],
            'data' => $preparedData,
        ];

        echo json_encode($json);
    }

    public function edit()
    {
        $id = (int)$this->request->get['id'];
        if (!empty($id) && !Auth::isLogged()) {
            Util::redirectToLoginPage();
        }
        $task = $this->repository->getById($id);
        $data['title'] = 'Edit Task';
        if (empty($task)) {
            $data['error_title'] = 'Task not found.';
            $data['not_found'] = true;
        } else {
            $taskModel = new Task($task);
            $user = $taskModel->getUser();
            $data['id'] = $id;
            $data['email'] = $user['email'];
            $data['user_name'] = $user['name'];
            $data['content'] = $taskModel->getContent();
            $data['form_action'] = Util::buildUrl(['p' => 'task', 'a' => 'save']);
            $data['completed'] = $taskModel->getCompleted();
            $data['error_title'] = $this->getRequestError();
        }

        echo $this->util->render('task', $data);
    }

    public function create()
    {
        $data['title'] = 'Create Task';
        $data['form_title'] = 'New Task';
        $data['form_action'] = Util::buildUrl(['p' => 'task', 'a' => 'save']);
        $data['error_title'] = $this->getRequestError();

        echo $this->util->render('task', $data);
    }

    public function save()
    {
        try {
            $id = (int)($this->request->post['id'] ?? 0);
            $content = $this->request->post['content'] ?? '';
            $completed = !empty($this->request->post['completed']);
            $taskModel = new Task();
            if ($id) {
                if (!Auth::isLogged()) {
                    Util::redirectToLoginPage();
                }
                $task = $this->repository->getById($id);
                if (empty($task)) {
                    Util::redirect(['p' => 'task', 'a' => 'edit', 'id' => $id]);
                }
                $taskModel->setData($task);
                $hasContentChanges = $taskModel->getContent() !== $content;
                if ($hasContentChanges || $taskModel->getCompleted() !== $completed) {
                    $taskModel->setUpdatedAt((new \DateTime())->format('Y:m:d H:i:s'));
                }
                $taskModel->setAdminUpdated($hasContentChanges);
                $taskModel->setCompleted($completed);

            } else {
                $userName = $this->request->post['name'] ?? null;
                $email = $this->request->post['email'] ?? null;

                //Save Task user
                $userRepository = new UserRepository();
                $user = $userRepository->getByEmail($email);
                if (empty($user)) {
                    $user = $userRepository->getByName($userName);
                }
                $userModel = new User($user);
                $userModel->setName($userName)->setEmail($email);
                $userModel->save();
                $taskModel->setUserId($userModel->getId());
                $taskModel->setUpdatedAt((new \DateTime())->format('Y:m:d H:i:s'));
            }
            $taskModel->setContent($content);
            $taskModel->save();

            Util::redirect();
        } catch (ValidationException $e) {
            $this->redirectToTask($id, $e->getErrorCode());
        } catch (\Throwable $t) {
            $this->redirectToTask($id, ValidationException::ERR_INTERNAL);
        }
    }

    private function redirectToTask(int $taskId = null, $error = null): void
    {
        $query = ['p' => 'task'];
        if (!empty($taskId)) {
            $query['a'] = 'edit';
            $query['id'] = $taskId;
        } else {
            $query['a'] = 'create';
        }
        if (!empty($error)) {
            $query['error'] = $error;
        }
        Util::redirect($query);
    }
}
