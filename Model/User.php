<?php

namespace App\Model;

use App\Core\Util;
use App\Exception\ValidationException;
use App\Repository\UserRepository;
use PDO;

class User extends BaseModel
{
    protected $repository = UserRepository::class;

    protected $table = 'user';

    protected $columns = [
        'id' => PDO::PARAM_INT,
        'name' => PDO::PARAM_STR,
        'email' => PDO::PARAM_STR,
        'password' => PDO::PARAM_STR,
        'is_admin' => PDO::PARAM_BOOL,
    ];

    /** @var int */
    private $id = 0;
    /** @var string */
    private $email = '';
    /** @var string */
    private $name = '';
    /** @var string */
    private $password = '';
    /** @var bool */
    private $isAdmin = 0;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    public function setId($id): User
    {
        $this->id = (int)$id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName($name): User
    {
        $this->name = (string)$name;

        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): User
    {
        $this->email = (string)$email;

        return $this;
    }

    public function getIsAdmin(): bool
    {
        return $this->isAdmin;
    }

    public function setIsAdmin($isAdmin): User
    {
        $this->isAdmin = (bool)$isAdmin;

        return $this;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): User
    {
        $this->password = $password;

        return $this;
    }

    public function validate()
    {
        parent::validate();
        if (empty($this->name)) {
            throw new ValidationException(ValidationException::ERR_EMPTY_NAME);
        }
        if (empty($this->email)) {
            throw new ValidationException(ValidationException::ERR_EMPTY_EMAIL);
        }
        if (!Util::isValidEmail($this->email)) {
            throw new ValidationException(ValidationException::ERR_INVALID_EMAIL);
        }
    }

}
