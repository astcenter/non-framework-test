<?php

namespace App\Repository;

use App\Core\Db;
use App\Model\Task;
use PDO;

class TaskRepository extends BaseRepository
{
    protected const MAX_LIMIT = 3;

    protected $model = Task::class;

    protected $orderFields = [
        'name',
        'email',
        'content',
        'completed',
        'admin_updated',
        'updated_at',
        'id',
    ];

    /**
     * @param array $filter
     * @param array $order
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public function getFiltered(
        array $filter = [],
        array $order = [],
        int $offset = 0,
        int $limit = self::MAX_LIMIT
    ): array {
        $table = $this->getTableName();
        $where = '';
        foreach ($filter as $parameters) {
            $field = $parameters['field'] ?? null;
            $value = $parameters['value'] ?? null;
            if (!isset($field, $value)) {
                continue;
            }
            $operator = $parameters['operator'] ?? self::OPERATOR_EQUAL;
            $tablePrefix = 't';
            $where = "AND {$tablePrefix}.{$field} {$operator} {$value}}".self::PARAMETER_SEPARATOR;
        }
        $where = !empty($where) ? 'WHERE '.rtrim($where, self::PARAMETER_SEPARATOR) : '';

        $orderStatement = '';
        foreach ($order as $parameters) {
            $field = $parameters['column'];
            $direction = strtoupper($parameters['dir'] ?? null) === self::ORDER_DIRECTION_ASC
                ? self::ORDER_DIRECTION_ASC : self::ORDER_DIRECTION_DESC;
            $orderStatement .= "{$field} {$direction}".self::PARAMETER_SEPARATOR;
        }
        $orderStatement = !empty($orderStatement) ? 'ORDER BY '.rtrim($orderStatement, self::PARAMETER_SEPARATOR) : '';

        $query = "SELECT SQL_CALC_FOUND_ROWS 
            t.id, 
            t.content, 
            u.name, 
            u.email, 
            t.completed, 
            t.admin_updated,
            t.updated_at FROM {$table} t
        INNER JOIN user u ON u.id = t.user_id
        {$where}
        {$orderStatement}
        LIMIT {$offset}, {$limit}";

        $stmt = $this->db->prepare($query);
        foreach ($filter as $parameters) {
            $field = $parameters['field'];
            $value = $parameters['value'];
            if (!isset($field, $value)) {
                continue;
            }
            $stmt->bindParam(Db::getPlaceholder($field), $value, $this->getColumnType($field) ?? PDO::PARAM_STR);
        }

        $stmt->execute();

        return [
            'data' => $stmt->fetchAll(PDO::FETCH_ASSOC),
            'total' => $this->db->query('SELECT FOUND_ROWS()')->fetchColumn(),
        ];
    }
}
