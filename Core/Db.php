<?php

namespace App\Core;

use PDO;

class Db extends PDO
{
    private const PLACEHOLDER_SYMBOL = ':';
    private const PLACEHOLDER_SEPARATOR = ', ';

    public function __construct()
    {
        $host = Util::env('DB_HOST');
        $db = Util::env('DB_NAME');
        $user = Util::env('DB_USR');
        $pwd = Util::env('DB_PWD');
        $charset = 'utf8';

        $dsn = "mysql:host={$host};dbname={$db};charset={$charset}";
        $opt = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        ];

        parent::__construct($dsn, $user, $pwd, $opt);
    }

    public static function preparePlaceholders(array $parameters)
    {
        $result = '';
        foreach ($parameters as $parameter) {
            $result .= self::PLACEHOLDER_SYMBOL.$parameter.self::PLACEHOLDER_SEPARATOR;
        }

        return rtrim($result, self::PLACEHOLDER_SEPARATOR);
    }

    /**
     * @param array $fields
     * @return string
     */
    public static function prepareFieldNames(array $fields)
    {
        $result = '';
        foreach ($fields as $field) {
            $result .= $field.self::PLACEHOLDER_SEPARATOR;
        }

        return rtrim($result, self::PLACEHOLDER_SEPARATOR);
    }

    /**
     * @param array $data
     * @return string
     */
    public static function prepareUpdatePlaceholders(array $data): string
    {
        $result = '';
        foreach ($data as $key => $value) {
            $placeholder = self::getPlaceholder($key);
            $result .= "{$key} = {$placeholder}".self::PLACEHOLDER_SEPARATOR;
        }

        return rtrim($result, self::PLACEHOLDER_SEPARATOR);
    }

    /**
     * @param $field
     * @return string
     */
    public static function getPlaceholder($field): string
    {
        return self::PLACEHOLDER_SYMBOL.$field;
    }

}
