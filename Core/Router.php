<?php

namespace App\Core;

use App\Controller\BaseController;
use App\Controller\ControllerInterface;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;

class Router
{
    private const CONTROLLER_NAMESPACE = 'App\Controller\\';
    private const CONTROLLER_DEFAULT_NAME = 'Task';
    private const CONTROLLER_DEFAULT_ACTION = 'index';

    public function run(array $parameters): void
    {
        $controllerName = !empty($parameters['controller']) ? $parameters['controller'] : self::CONTROLLER_DEFAULT_NAME;
        $action = !empty($parameters['action']) ? $parameters['action'] : self::CONTROLLER_DEFAULT_ACTION;
        $controllerClass = self::getControllerClass($controllerName);
        try {
            /** @var ControllerInterface $controller */
            $controller = new $controllerClass();
            if ((new ReflectionClass($controllerClass))->hasMethod($action) && (new ReflectionMethod($controllerClass,
                    $action))->isPublic()) {
                $controller->{$action}();
            } else {
                $controller->notFound();
            }
        } catch (\Throwable $e) {
            (new BaseController())->notFound();
        }
    }

    /**
     * @param string $name
     * @return string
     */
    private static function getControllerClass(string $name): string
    {
        return self::CONTROLLER_NAMESPACE.Util::camelize($name).'Controller';
    }
}
