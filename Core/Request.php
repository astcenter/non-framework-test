<?php


namespace App\Core;


class Request
{
    public $get;

    public $post;

    public function __construct(array $get = [], array $post = [])
    {
        $this->get = $get;
        $this->post = $post;
    }
}