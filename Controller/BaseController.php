<?php

namespace App\Controller;

use App\Core\Auth;
use App\Core\Request;
use App\Core\Util;
use App\Exception\ValidationException;
use Twig\Error\LoaderError as TwigLoaderError;
use Twig\Error\RuntimeError as TwigRuntimeError;
use Twig\Error\SyntaxError as TwigSyntaxError;

class BaseController implements ControllerInterface
{
    /** @var Util */
    protected $util;

    /** @var Auth */
    protected $auth;

    /** @var Request */
    protected $request;

    public function __construct()
    {
        $this->auth = new Auth();
        $this->util = new Util();
        $this->request = new Request($_GET, $_POST);
    }

    /**
     * @throws TwigLoaderError
     * @throws TwigRuntimeError
     * @throws TwigSyntaxError
     */
    public function notFound(): void
    {
        echo $this->util->render('not_found');
    }

    /**
     * @return string
     */
    protected function getRequestError(): string
    {
        $error = $this->request->get['error'] ?? null;
        if (empty($error)) {
            return '';
        }
        switch ($error) {
            case ValidationException::ERR_EMPTY_NAME:
                $error = 'User Name is required';
                break;
            case ValidationException::ERR_EMPTY_EMAIL:
                $error = 'E-mail is required';
                break;
            case ValidationException::ERR_INVALID_EMAIL:
                $error = 'Invalid E-mail';
                break;
            case ValidationException::ERR_CONTENT:
                $error = 'Task content required';
                break;
            case ValidationException::ERR_EMPTY_PASSWORD:
                $error = 'Password is required';
                break;
            case ValidationException::ERR_INCORRECT_CREDENTIALS:
                $error = 'Incorrect credentials';
                break;
            default:
                $error = 'Internal error';
                break;
        }

        return $error;
    }
}
