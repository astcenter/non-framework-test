<?php

namespace App\Model;

use App\Core\{Db, Util,};
use App\Repository;
use App\Repository\RepositoryInterface;
use PDO;

class BaseModel implements ModelInterface
{
    /** @var Db */
    protected $db;

    /** @var string */
    protected $table;

    /** @var array */
    protected $columns = [];

    protected $primaryKey = 'id';

    /** @var string */
    protected $repository;

    /** @var array */

    public function __construct(array $data = [])
    {
        $this->db = new Db;
        $this->setData($data);
    }


    /**
     * @return array
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

    /**
     * @return string
     */
    public function getTableName(): string
    {
        return $this->table;
    }

    /**
     * @return string
     */
    public function getPrimaryKey(): string
    {
        return $this->primaryKey;
    }

    /**
     * @param array $data
     * @return self
     */
    public function setData(array $data = []): self
    {
        foreach ($data as $key => $value) {
            $methodSuffix = Util::camelize($key);
            $setMethod = "set{$methodSuffix}";
            if (!method_exists($this, $setMethod)) {
                continue;
            }
            $this->$setMethod($value);
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        $data = [];
        foreach ($this->columns as $column => $type) {
            $getMethod = 'get'.Util::camelize($column);
            if (method_exists($this, $getMethod)) {
                $data[$column] = $this->$getMethod();
                continue;
            }
            $property = Util::camelize($column, false);
            if (!property_exists($this, $property)) {
                continue;
            }
            $data[$column] = $this->$property;
        }

        return $data;
    }


    public function save()
    {
        $this->validate();
        $data = $this->getData();
        $id = $data[$this->primaryKey] ?? null;
        if (!empty($id)) {
            unset($data[$this->primaryKey]);
            $this->update($id, $data);
        } else {
            $id = $this->create($data);
        }

        $this->setData($this->getRepository()->getById($id));

        return $this;
    }

    /**
     * @param array $data
     * @return int
     */
    public function create(array $data): int
    {
        $fields = array_keys($data);
        $fieldNames = Db::prepareFieldNames($fields);
        $placeholders = Db::preparePlaceholders($fields);
        $stmt = $this->db->prepare("INSERT INTO {$this->table} ({$fieldNames}) VALUES ({$placeholders})");
        foreach ($data as $key => $value) {
            $stmt->bindValue(Db::getPlaceholder($key), $value, $this->columns[$key] ?? PDO::PARAM_STR);
        }
        $stmt->execute();

        return $this->db->lastInsertId();
    }

    /**
     * @param $id
     * @param array $data
     * @return bool
     */
    public function update($id, array $data): bool
    {
        $placeholders = Db::prepareUpdatePlaceholders($data);
        $stmt = $this->db->prepare("UPDATE {$this->table} SET {$placeholders} WHERE {$this->primaryKey} = :{$this->primaryKey}");
        $stmt->bindValue($this->primaryKey, $id, $this->columns[$this->primaryKey] ?? PDO::PARAM_INT);
        foreach ($data as $key => $value) {
            $stmt->bindValue(Db::getPlaceholder($key), $value, $this->columns[$key] ?? PDO::PARAM_STR);
        }

        return $stmt->execute();
    }

    /**
     * @param $id
     * @return bool
     */
    public function delete($id): bool
    {
        $placeholder = Db::getPlaceholder($id);
        $stmt = $this->db->prepare("DELETE FROM {$this->table} WHERE {$this->primaryKey} = :{$placeholder}");
        $stmt->bindParam($placeholder, $id, $this->columns[$this->primaryKey] ?? PDO::PARAM_INT);

        return $stmt->execute();
    }

    /**
     * @param $column
     * @return int
     */
    public function getColumnType($column): int
    {
        return $this->columns[$column] ?? null;
    }

    /**
     * @return RepositoryInterface
     */
    protected function getRepository(): RepositoryInterface
    {
        $className = $this->repository;

        return new $className();
    }

    public function validate()
    {
    }
}
