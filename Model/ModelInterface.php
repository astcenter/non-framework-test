<?php


namespace App\Model;


interface ModelInterface
{
    public function getTableName(): string;

    public function getPrimaryKey(): string;

    public function getColumnType(string $column): int;

    public function getColumns(): array;

    public function validate();

}