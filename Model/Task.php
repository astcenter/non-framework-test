<?php

namespace App\Model;

use App\Exception\ValidationException;
use App\Repository\TaskRepository;
use App\Repository\UserRepository;
use PDO;

class Task extends BaseModel
{
    protected $repository = TaskRepository::class;

    protected $table = 'task';

    /** @var int */
    private $id = 0;
    /** @var int */
    private $userId = 0;
    /** @var string */
    private $title = '';
    /** @var string */
    private $content = '';
    /** @var bool */
    private $completed = 0;
    /** @var bool */
    private $adminUpdated = 0;
    /** @var string */
    private $updatedAt = '';

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    public function setId($id): Task
    {
        $this->id = (int)$id;

        return $this;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function setUserId($userId): Task
    {
        $this->userId = (int)$userId;

        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle($title): Task
    {
        $this->title = (string)$title;

        return $this;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent($content): Task
    {
        $this->content = (string)$content;

        return $this;
    }

    public function getCompleted(): bool
    {
        return $this->completed;
    }

    public function setCompleted($completed): Task
    {
        $this->completed = (bool)$completed;

        return $this;
    }

    public function getAdminUpdated(): bool
    {
        return $this->adminUpdated;
    }

    public function setAdminUpdated($adminUpdated): Task
    {
        $this->adminUpdated = (bool)$adminUpdated;

        return $this;
    }


    public function getUpdatedAt(): string
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt($updatedAt): Task
    {
        $this->updatedAt = (string)$updatedAt;

        return $this;
    }

    /**
     * @return array|mixed
     */
    public function getUser()
    {
        $userId = $this->getUserId();
        if (empty($userId)) {
            return [];
        }
        $userRepository = new UserRepository();

        return $userRepository->getById($userId);
    }

    public function validate()
    {
        parent::validate();

        if (!$this->content) {
            throw new ValidationException(ValidationException::ERR_CONTENT);
        }
    }

    protected $columns = [
        'id' => PDO::PARAM_INT,
        'user_id' => PDO::PARAM_INT,
        'content' => PDO::PARAM_STR,
        'completed' => PDO::PARAM_BOOL,
        'admin_updated' => PDO::PARAM_BOOL,
        'updated_at' => PDO::PARAM_STR,
    ];
}
