<?php

namespace App\Core;

use App\Exception\ValidationException;
use Twig\{Environment, Loader\FilesystemLoader as TwigLoader, Environment as TwigEnvironment};

class Util
{
    /** @var Environment */
    private $twig;

    public function __construct()
    {
        $this->initTwig();
    }

    /**
     * @param array $parameters
     * @return string
     */
    public static function buildUrl(array $parameters = []): string
    {
        $url = ROOT_URL.'/';
        if (empty($parameters)) {
            return $url;
        }

        return $url.'?'.urldecode(http_build_query($parameters));
    }


    /**
     * @param string $viewName
     * @param array $parameters
     * @return string
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \Twig\Error\LoaderError
     */
    public function render(string $viewName = 'index', array $parameters = []): string
    {
        $parameters['root_url'] = ROOT_URL;
        $parameters['login_url'] = self::buildUrl(['p' => 'auth', 'a' => 'index']);
        $parameters['login_action_url'] = self::buildUrl(['p' => 'auth', 'a' => 'login']);
        $parameters['logout_url'] = self::buildUrl(['p' => 'auth', 'a' => 'logout']);
        $parameters['task_list_url'] = self::buildUrl(['p' => 'task', 'a' => 'list']);
        $parameters['is_logged'] = (int)Auth::isLogged();

        return $this->twig->render($viewName.'.twig', $parameters);
    }

    /**
     * @param $str
     * @param bool $capitalizeFirst
     * @return string|null
     */
    public static function camelize($str, $capitalizeFirst = true): ?string
    {
        if (empty($str[0])) {
            return '';
        }
        $str = strtolower($str);
        if ($capitalizeFirst) {
            $str[0] = strtoupper($str[0]);
        }

        return preg_replace_callback(
            '/_([a-z])/',
            static function ($m) {
                return strtoupper($m[1]);
            },
            $str
        );
    }

    /**
     * @param $key
     * @param null $default
     * @return array|false|string|null
     */
    public static function env($key, $default = null)
    {
        $value = getenv($key);
        if ($value === false) {
            return $default;
        }

        return $value;
    }

    public static function isValidEmail(string $email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    public static function redirect(array $parameters = [])
    {
        $redirectLocation = self::buildUrl($parameters);
        header("Location: {$redirectLocation}");
        die();
    }

    public static function redirectToLoginPage()
    {
        self::redirect(['p' => 'auth', 'a' => 'index']);
    }

    /**
     * @return $this
     */
    private function initTwig(): self
    {
        $viewPath = ROOT_PATH.'View/';

        $loader = new TwigLoader($viewPath);
        $this->twig = new TwigEnvironment($loader, [
            'cache' => ROOT_PATH.'var/cache',
        ]);

        return $this;
    }
}
