// Call the dataTables jQuery plugin
$(document).ready(function () {
    var tableOptions = {
        processing: true,
        serverSide: true,
        ajax: "./?p=task&a=list",
        order: [5, 'desc'],
        columns: [
            {data: 'name'},
            {data: 'email'},
            {data: 'content'},
            {data: 'completed'},
            {data: 'admin_updated'},
            {data: 'updated_at'},
        ],
        lengthMenu: [3, 6],
        searching: false,
        rowCallback: function (row, data) {
            if (data.completed) {
                $(row).addClass('highlight');
            } else if (data.admin_updated) {
                $(row).addClass('highlight-orange');
            }
        }
    };
    if (globalConfig.is_logged) {
        tableOptions.columns.push({data: 'id'});
    }

    $('#dataTable').DataTable(tableOptions);
});