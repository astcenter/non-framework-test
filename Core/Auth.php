<?php

namespace App\Core;

class Auth
{
    private const BCRYPT_COST = 10;

    public function __construct()
    {
    }

    /**
     * Hashes provided password with Bcrypt
     * @param string $password
     * @return string
     */
    public static function getHash($password): string
    {
        return password_hash($password, PASSWORD_BCRYPT, ['cost' => self::BCRYPT_COST]);
    }

    public static function verifyPassword($password, $hash): bool
    {
        return password_verify($password, $hash);
    }

    /**
     * @return bool
     */
    public static function isLogged(): bool
    {
        return !empty($_SESSION['user_context']);
    }

    /**
     * @param string $password
     * @param string $passwordHash
     * @return bool
     */
    public static function isPasswordCorrect(string $password, string $passwordHash): bool
    {
        return password_verify($password, $passwordHash);
    }

    public static function logIn(array $user): void
    {
        $_SESSION['user_context'] = $user;
    }

    public static function logOut(): void
    {
        if (isset($_SESSION['user_context'])) {
            unset($_SESSION['user_context']);
        }
    }
}
