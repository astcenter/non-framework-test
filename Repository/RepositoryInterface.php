<?php

namespace App\Repository;

interface RepositoryInterface
{
    public function getById($id);

    public function getFiltered(array $filter, array $order, int $offset, int $limit);
}