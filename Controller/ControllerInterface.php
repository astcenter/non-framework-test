<?php


namespace App\Controller;


interface ControllerInterface
{
    public function notFound();
}