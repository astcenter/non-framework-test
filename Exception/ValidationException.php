<?php

namespace App\Exception;

use RuntimeException;
use Throwable;

class ValidationException extends RuntimeException
{
    public const ERR_EMPTY_NAME = 'user_name';
    public const ERR_EMPTY_EMAIL = 'user_email';
    public const ERR_EMPTY_PASSWORD = 'user_password';
    public const ERR_INVALID_EMAIL = 'user_email_invalid';
    public const ERR_CONTENT = 'task_content';
    public const ERR_INTERNAL = 'internal';
    public const ERR_INCORRECT_CREDENTIALS = 'incorrect_credentials';


    /**
     * @var integer|null
     */
    private $errorCode;

    protected $message = 'Validation Error';

    public function __construct($code = self::ERR_INTERNAL, $message = 'Validation Error')
    {
        $this->errorCode = $code;
        parent::__construct($message);
    }

    public function getErrorCode()
    {
        return $this->errorCode;
    }
}