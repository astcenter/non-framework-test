<?php

namespace App\Repository;

use App\Model\User;
use PDO;

class UserRepository extends BaseRepository
{
    protected $model = User::class;

    /**
     * @param string $name
     * @param bool $isAdmin
     * @return array
     */
    public function getByName(string $name, bool $isAdmin = false): array
    {
        $table = $this->getTableName();
        $query = "SELECT t.* FROM {$table} t WHERE t.name = :name AND t.is_admin = :is_admin";
        $stmt = $this->db->prepare($query);
        $stmt->bindParam(':name', $name, PDO::PARAM_STR);
        $stmt->bindParam(':is_admin', $isAdmin, PDO::PARAM_BOOL);
        $stmt->execute();

        return $stmt->fetch(\PDO::FETCH_ASSOC) ?: [];
    }

    /**
     * @param string $email
     * @param bool $isAdmin
     * @return array
     */
    public function getByEmail(string $email, bool $isAdmin = false): array
    {
        $table = $this->getTableName();
        $query = "SELECT t.* FROM {$table} t WHERE t.email = :email AND t.is_admin = :is_admin";
        $stmt = $this->db->prepare($query);
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->bindParam(':is_admin', $isAdmin, PDO::PARAM_BOOL);
        $stmt->execute();

        return $stmt->fetch(\PDO::FETCH_ASSOC) ?: [];
    }
}
