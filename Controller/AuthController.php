<?php

namespace App\Controller;

use App\Core\Auth;
use App\Core\Util;
use App\Exception\AuthException;
use App\Exception\ValidationException;
use App\Repository\TaskRepository;
use App\Repository\UserRepository;

class AuthController extends BaseController
{
    /** @var UserRepository */
    private $repository;

    public function __construct()
    {
        parent::__construct();

        $this->repository = new UserRepository();
    }

    public function index()
    {
        echo $this->util->render('login', ['error_title' => $this->getRequestError()]);
    }

    public function login()
    {
        try {
            $name = $this->request->post['name'] ?? null;
            $password = $this->request->post['password'] ?? null;
            if (empty($name)) {
                throw new ValidationException(ValidationException::ERR_EMPTY_NAME);
            }
            if (empty($password)) {
                throw new ValidationException(ValidationException::ERR_EMPTY_PASSWORD);
            }

            $user = $this->repository->getByName($name, true);
            if (empty($user) || !Auth::isPasswordCorrect($password, $user['password'])) {
                throw new ValidationException(ValidationException::ERR_INCORRECT_CREDENTIALS);
            }
            Auth::logIn($user);
            Util::redirect();
        } catch (ValidationException $e) {
            Util::redirect(['p' => 'auth', 'a' => 'index', 'error' => $e->getErrorCode()]);
        } catch (\Throwable $t) {
            Util::redirect(['p' => 'auth', 'a' => 'index', 'error' => ValidationException::ERR_INTERNAL]);
        }

    }

    public function logout()
    {
        if (!Auth::isLogged()) {
            Util::redirect();
        }
        Auth::logOut();
        Util::redirect();
    }
}
