<?php

namespace App;

use App\Core;

$https = $_SERVER['HTTPS'] ?? null;
define('PROT', strtolower($https) === 'on' ? 'https://' : 'http://');

$https = $_SERVER['HTTPS'] ?? null;
$protocol = !empty($https) && $https !== 'off' ? 'https' : 'http';
$rootUrl =  sprintf(
        '%s://%s',
        $protocol,
        $_SERVER['SERVER_NAME']
    );
define('ROOT_URL', $rootUrl);

$rootPath = rtrim(__DIR__, '/').'/';
define('ROOT_PATH', $rootPath);
session_start();

try {
    require ROOT_PATH.'Core/Loader.php';
    require ROOT_PATH.'vendor/autoload.php';
    require ROOT_PATH.'env.php';

    $loader = Core\Loader::getInstance();
    $loader->register();

    $parameters = [
        'controller' => (!empty($_GET['p']) ? $_GET['p'] : 'task'),
        'action' => (!empty($_GET['a']) ? $_GET['a'] : 'index'),
    ];

    $router = new Core\Router();
    $router->run($parameters);
} catch (\Throwable $t) {
    echo $t->getMessage().':'.$t->getFile().':'.$t->getLine().PHP_EOL;
    echo '<pre>';
    echo $t->getTraceAsString();
}
