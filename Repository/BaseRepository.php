<?php

namespace App\Repository;

use App\Core\Db;
use App\Model\ModelInterface;
use PDO;

class BaseRepository implements RepositoryInterface
{
    protected const PARAMETER_SEPARATOR = ', ';

    protected const MAX_LIMIT = 10;

    protected const ORDER_DIRECTION_ASC = 'ASC';
    protected const ORDER_DIRECTION_DESC = 'DESC';

    protected const OPERATOR_EQUAL = '=';

    protected $db;

    protected $orderFields = [];

    /** @var string */
    protected $model;

    protected $maxLimit = 20;

    public function __construct()
    {
        $this->db = new Db;
    }

    /**
     * @param $id
     * @return array
     */
    public function getById($id): array
    {
        $table = $this->getTableName();
        $primaryKey = $this->getPrimaryKey();
        $placeholder = Db::getPlaceholder($primaryKey);
        $oStmt = $this->db->prepare("SELECT t.* FROM {$table} t WHERE t.{$primaryKey} = {$placeholder}");
        $oStmt->bindParam($placeholder, $id, $this->getColumnType($primaryKey) ?? \PDO::PARAM_INT);
        $oStmt->execute();

        return $oStmt->fetch(\PDO::FETCH_ASSOC) ?: [];
    }

    /**
     * @param array $filter
     * @param array $order
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public function getFiltered(
        array $filter = [],
        array $order = [],
        int $offset = 0,
        int $limit = self::MAX_LIMIT
    ): array {
        $table = $this->getTableName();
        $where = '';
        foreach ($filter as $parameters) {
            $field = $parameters['field'];
            $operator = $parameters['operator'];
            $value = $parameters['value'];
            $where = "AND p.{$field} {$operator} {$value}}".self::PARAMETER_SEPARATOR;
        }
        $where = !empty($where) ? 'WHERE '.rtrim($where, self::PARAMETER_SEPARATOR) : '';

        $orderStatement = '';
        foreach ($order as $parameters) {
            $field = $parameters['field'];
            $direction = ($parameters['dir'] ?? null) === self::ORDER_DIRECTION_ASC ? self::ORDER_DIRECTION_ASC : self::ORDER_DIRECTION_DESC;
            $orderStatement .= "{$field} {$direction}".self::PARAMETER_SEPARATOR;
        }
        $orderStatement = !empty($orderStatement) ? 'ORDER BY '.rtrim($orderStatement) : '';

        $query = "SELECT t.* FROM {$table} t
        {$where}
        {$orderStatement}
        LIMIT {$offset}, {$limit}";

        $stmt = $this->db->prepare($query);
        foreach ($filter as $parameters) {
            $field = $parameters['field'];
            $value = $parameters['value'];
            $stmt->bindParam(Db::getPlaceholder($field), $value, $this->getColumnType($field) ?? PDO::PARAM_STR);
        }
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getOrderFields(): array
    {
        return $this->orderFields;
    }

    /**
     * @return ModelInterface
     */
    protected function getModel(): ModelInterface
    {
        $modelClass = $this->model;

        return new $modelClass();
    }

    /**
     * @return string
     */
    protected function getTableName(): string
    {
        return $this->getModel()->getTableName();
    }

    /**
     * @return string
     */
    protected function getPrimaryKey(): string
    {
        return $this->getModel()->getPrimaryKey();
    }

    /**
     * @param $column
     * @return int
     */
    protected function getColumnType($column): int
    {
        return $this->getModel()->getColumnType($column);
    }
}
