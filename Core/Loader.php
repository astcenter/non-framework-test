<?php

namespace App\Core;

class Loader
{

    protected static $instance;

    final private function __construct()
    {
    }

    final private function __clone()
    {
    }

    /**
     * Get instance of class.
     *
     * @access public
     * @static
     * @return object
     */
    public static function getInstance()
    {
        return static::$instance ?? static::$instance = new static;
    }

    private const NAMESPACE_PREFIX = 'App\\';

    public function register(): void
    {
        // Register the loader method
        spl_autoload_register([$this, 'loadClass']);
    }

    private function loadClass($class): void
    {
        $baseDir = ROOT_PATH;

        // does the class use the namespace prefix?
        $len = strlen(self::NAMESPACE_PREFIX);
        if (strncmp(self::NAMESPACE_PREFIX, $class, $len) !== 0) {
            // no, move to the next registered autoloader
            return;
        }

        $relativeClass = substr($class, $len);
        $file = $baseDir.str_replace('\\', '/', $relativeClass).'.php';
        if (!file_exists($file)) {
            return;
        }

        require_once $file;
    }
}
